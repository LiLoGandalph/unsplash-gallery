import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/photo_list_bloc.dart';
import 'repositories/photo/photos_repository.dart';
import 'repositories/photo/unsplash/unsplash_api.dart';
import 'widgets/pages/photos_list/photos_list.dart';

const _id = 'ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(UnsplashGallery());
}

class UnsplashGallery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider<PhotosRepository>(
      create: (context) => UnsplashApi(id: _id, photosPerPage: 30),
      child: BlocProvider(
        create: (context) =>
            PhotosListBloc(context.repository<PhotosRepository>())
              ..add(const Init()),
        child: Builder(
          builder: (context) {
            return MaterialApp(
              title: 'Unsplash Gallery',
              theme: ThemeData(
                primaryColor: Colors.pink,
                accentColor: Colors.pink,
                backgroundColor: Colors.transparent,
                visualDensity: VisualDensity.adaptivePlatformDensity,
              ),
              home: PhotosList(photosBloc: context.bloc<PhotosListBloc>()),
            );
          },
        ),
      ),
    );
  }
}

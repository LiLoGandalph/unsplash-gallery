import 'package:flutter/material.dart';

import 'user.dart';

class Photo {
  Photo({
    @required this.id,
    @required this.createdAt,
    @required this.updatedAt,
    @required this.width,
    @required this.height,
    @required this.color,
    @required this.likes,
    @required this.description,
    @required this.altDescription,
    @required this.user,
    @required this.urls,
    @required this.isSponsored,
  });

  Photo.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        createdAt = DateTime.parse(json['created_at']),
        updatedAt = DateTime.parse(json['updated_at']),
        width = json['width'],
        height = json['height'],
        color = Color(int.parse(json['color'].replaceRange(0, 1, '0xFF'))),
        likes = json['likes'],
        description = json['description'],
        altDescription = json['alt_description'],
        user = User.fromJson(json['user']),
        urls = PhotoUrls.fromJson(json['urls']),
        isSponsored = json['sponsorship'] != null;

  final String id;

  final DateTime createdAt;
  final DateTime updatedAt;

  final int width;
  final int height;
  double get aspectRatio => width / height;

  final Color color;

  final int likes;

  final String description;
  final String altDescription;

  final User user;

  final PhotoUrls urls;

  final bool isSponsored;
}

class PhotoUrls {
  const PhotoUrls({
    @required this.raw,
    @required this.full,
    @required this.regular,
    @required this.small,
    @required this.thumb,
  });

  PhotoUrls.fromJson(Map<String, dynamic> json)
      : raw = Uri.parse(json['raw']),
        full = Uri.parse(json['full']),
        regular = Uri.parse(json['regular']),
        small = Uri.parse(json['small']),
        thumb = Uri.parse(json['thumb']);

  final Uri raw;
  final Uri full;
  final Uri regular;
  final Uri small;
  final Uri thumb;

  Uri fullWithCustomDpr({
    @required double dpr,
    double width,
    double height,
  }) {
    final newQuery = <String, String>{}
      ..addAll(full.queryParameters)
      ..addAll({
        if (width != null) 'w': width.toString(),
        if (height != null) 'h': height.toString(),
        if (dpr != null) 'dpr': dpr.toString(),
      });
    return full.replace(queryParameters: newQuery);
  }
}

import 'package:meta/meta.dart';

class User {
  const User({
    @required this.id,
    @required this.username,
    @required this.name,
    @required this.profileImage,
  });

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        username = json['username'],
        name = json['name'],
        profileImage = UserProfileImage.fromJson(json['profile_image']);

  final String id;
  final String username;
  final String name;
  final UserProfileImage profileImage;
}

class UserProfileImage {
  const UserProfileImage({
    @required this.small,
    @required this.medium,
    @required this.large,
  });

  UserProfileImage.fromJson(Map<String, dynamic> json)
      : small = Uri.parse(json['small']),
        medium = Uri.parse(json['medium']),
        large = Uri.parse(json['large']);

  final Uri small;
  final Uri medium;
  final Uri large;
}

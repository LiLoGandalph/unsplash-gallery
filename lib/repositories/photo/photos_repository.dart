import '../../models/photo.dart';

abstract class PhotosRepository {
  int get photosPerPage;
  Future<PhotosData> getPhotos([int photoIndex = 0]);
  void dispose();
}

class PhotosData {
  const PhotosData(this.totalPhotos, this.photos);

  final int totalPhotos;
  final List<Photo> photos;
}

import 'dart:convert';
import 'dart:io';

import 'package:meta/meta.dart';

import '../../../models/photo.dart';
import '../photos_repository.dart';

class UnsplashApi implements PhotosRepository {
  UnsplashApi({
    @required this.id,
    this.photosPerPage = 10,
    this.sortOrder = PhotoSortOrder.latest,
  }) : _http = HttpClient();

  final String id;
  final PhotoSortOrder sortOrder;
  @override
  final int photosPerPage;

  final HttpClient _http;

  static const _scheme = 'https';
  static const _host = 'api.unsplash.com';

  @override
  Future<PhotosData> getPhotos([int photoIndex = 0]) async {
    try {
      final request = await _http.getUrl(
        Uri(
          scheme: _scheme,
          host: _host,
          path: 'photos',
          queryParameters: {
            'order_by': sortOrder.toJson(),
            'per_page': photosPerPage.toString(),
            'page': ((photoIndex + 1) / photosPerPage).ceil().toString(),
          },
        ),
      );
      request.headers.set(HttpHeaders.acceptHeader, 'v1');
      request.headers.set(HttpHeaders.authorizationHeader, 'Client-ID $id');

      final response = await request.close();
      final totalPhotos = int.parse(response.headers.value('X-Total'));
      final rawJson = await utf8.decodeStream(response);
      final List<dynamic> parsedJson = json.decode(rawJson);
      final photos =
          List<Photo>.of(parsedJson.map((json) => Photo.fromJson(json)));
      return PhotosData(totalPhotos, photos);
    } on SocketException {
      return null;
    }
  }

  @override
  void dispose() {
    _http.close(force: true);
  }
}

enum PhotoSortOrder {
  latest,
  oldest,
  popular,
}

extension PhotoSortOrderExtension on PhotoSortOrder {
  String toJson() {
    switch (this) {
      case PhotoSortOrder.latest:
        return 'latest';
        break;
      case PhotoSortOrder.oldest:
        return 'oldest';
        break;
      case PhotoSortOrder.popular:
        return 'popular';
        break;
      default:
        throw UnimplementedError();
    }
  }
}

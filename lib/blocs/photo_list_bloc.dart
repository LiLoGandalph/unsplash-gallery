import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../models/photo.dart';
import '../repositories/photo/photos_repository.dart';

class PhotoListEvent {}

class Init implements PhotoListEvent {
  const Init();
}

class ShowingPhoto implements PhotoListEvent {
  const ShowingPhoto(this.photoIndex);
  final int photoIndex;
}

class RefreshPhotos implements PhotoListEvent {
  const RefreshPhotos();
}

class RefreshMorePhotos implements PhotoListEvent {
  const RefreshMorePhotos();
}

class PhotosListState {
  const PhotosListState({
    @required this.photosStatus,
    @required this.photos,
    @required this.loadingMorePhotos,
  });

  final LoadingStatus photosStatus;
  final List<Photo> photos;

  final LoadingStatus loadingMorePhotos;
}

class PhotosListBloc extends Bloc<PhotoListEvent, PhotosListState> {
  PhotosListBloc(this.photosRepository)
      : _loadingThreshold = (photosRepository.photosPerPage * 0.3).ceil(),
        super(
          const PhotosListState(
            photosStatus: LoadingStatus.loading,
            photos: [],
            loadingMorePhotos: LoadingStatus.loaded,
          ),
        );

  final PhotosRepository photosRepository;

  List<Photo> _loadedPhotos = [];
  int _totalPhotos;

  final int _loadingThreshold;

  @override
  Stream<PhotosListState> mapEventToState(PhotoListEvent event) async* {
    if (event is Init) {
      yield const PhotosListState(
        photosStatus: LoadingStatus.loading,
        photos: [],
        loadingMorePhotos: LoadingStatus.loaded,
      );
      yield await _loadPhotos();
    } else if (event is ShowingPhoto) {
      if (event.photoIndex >= _loadedPhotos.length - _loadingThreshold &&
          _loadedPhotos.length < _totalPhotos &&
          state.loadingMorePhotos == LoadingStatus.loaded) {
        yield await _loadPhotos(withIndex: _loadedPhotos.length);
      }
    } else if (event is RefreshPhotos) {
      _loadedPhotos = [];
      yield await _loadPhotos();
    } else if (event is RefreshMorePhotos) {
      yield PhotosListState(
        photosStatus: LoadingStatus.loaded,
        photos: _loadedPhotos,
        loadingMorePhotos: LoadingStatus.loading,
      );
      yield await _loadPhotos(withIndex: _loadedPhotos.length + 1);
    }
  }

  Future<PhotosListState> _loadPhotos({int withIndex}) async {
    final photosData = await photosRepository
        .getPhotos(withIndex ?? 0)
        .catchError((_) => null);
    if (photosData != null) {
      _totalPhotos = photosData.totalPhotos;
      _loadedPhotos.addAll(photosData.photos);
      return PhotosListState(
        photosStatus: LoadingStatus.loaded,
        photos: _loadedPhotos,
        loadingMorePhotos: LoadingStatus.loaded,
      );
    } else {
      return PhotosListState(
        photosStatus:
            withIndex == null ? LoadingStatus.failed : LoadingStatus.loaded,
        photos: state.photos,
        loadingMorePhotos:
            withIndex == null ? LoadingStatus.loaded : LoadingStatus.failed,
      );
    }
  }

  @override
  Future<void> close() {
    photosRepository.dispose();
    return super.close();
  }
}

enum LoadingStatus {
  loading,
  loaded,
  failed,
}

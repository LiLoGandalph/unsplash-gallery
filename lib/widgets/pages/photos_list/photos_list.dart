import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/photo_list_bloc.dart';
import 'elements/photo_card.dart';

class PhotosList extends StatefulWidget {
  const PhotosList({
    @required this.photosBloc,
    Key key,
    this.title,
  }) : super(key: key);

  final String title;
  final PhotosListBloc photosBloc;

  @override
  _PhotosListState createState() => _PhotosListState();
}

class _PhotosListState extends State<PhotosList> {
  Completer _refreshCompleter = Completer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Unsplash Gallery'),
      ),
      body: Center(
        child: BlocConsumer<PhotosListBloc, PhotosListState>(
          listener: (context, state) {
            if (state.photosStatus != LoadingStatus.loading) {
              _completeRefresh();
            }
          },
          bloc: widget.photosBloc,
          builder: (context, state) {
            return RefreshIndicator(
              onRefresh: () {
                widget.photosBloc.add(const RefreshPhotos());
                return _refreshCompleter.future;
              },
              child: state.photosStatus == LoadingStatus.loaded
                  ? _buildLoaded(context, state)
                  : state.photosStatus == LoadingStatus.failed
                      ? _buildFailed(context)
                      : const CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }

  Widget _buildFailed(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: SizedBox.fromSize(
            size: constraints.biggest,
            child: Center(
              child: Text(
                'Something went wrong',
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildLoaded(BuildContext context, PhotosListState state) {
    final itemCount = state.loadingMorePhotos == LoadingStatus.loaded
        ? state.photos.length
        : state.photos.length + 1;
    return ListView.builder(
      cacheExtent: MediaQuery.of(context).size.height,
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
      itemCount: itemCount,
      itemBuilder: (context, index) {
        if (index == itemCount - 1) {
          if (state.loadingMorePhotos == LoadingStatus.loading) {
            return const SizedBox(
              height: 80,
              child: Center(child: CircularProgressIndicator()),
            );
          } else {
            return SizedBox(
              height: 80,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Something went wrong',
                      style: Theme.of(context).textTheme.subtitle2,
                    ),
                    IconButton(
                      iconSize: 36,
                      icon: const Icon(Icons.refresh),
                      onPressed: () =>
                          widget.photosBloc.add(const RefreshMorePhotos()),
                    ),
                  ],
                ),
              ),
            );
          }
        } else {
          widget.photosBloc.add(ShowingPhoto(index));
          return PhotoCard(
            key: ValueKey(state.photos[index].id),
            photo: state.photos[index],
          );
        }
      },
    );
  }

  void _completeRefresh() {
    _refreshCompleter.complete();
    _refreshCompleter = Completer();
  }
}

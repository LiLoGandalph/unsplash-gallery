import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../../models/photo.dart';
import '../../fullscreen_photo/fullscreen_photo.dart';
import '../../../common/photo/photo_placeholder.dart';

class PhotoCard extends StatelessWidget {
  const PhotoCard({
    @required this.photo,
    Key key,
  }) : super(key: key);

  final Photo photo;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final theme = Theme.of(context);
        final width = constraints.biggest.width;
        final dpr = MediaQuery.of(context).devicePixelRatio;
        final previewUrl = photo.urls
            .fullWithCustomDpr(
              dpr: dpr,
              width: width,
            )
            .toString();
        final userProfilePicture = SizedBox.fromSize(
          size: const Size.square(44),
          child: ClipOval(
            child: CachedNetworkImage(
              imageUrl: photo.user.profileImage.large.toString(),
            ),
          ),
        );
        final userNameAndPhotoDate = Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              photo.user.name,
              style: theme.textTheme.subtitle2,
            ),
            const SizedBox(height: 2),
            Text(
              // TODO: use intl or something and move date formatting there.
              '${photo.createdAt.year}/${photo.createdAt.month.toString().padLeft(2, '0')}/${photo.createdAt.day.toString().padLeft(2, '0')}',
              style: theme.textTheme.caption,
            ),
          ],
        );
        final likes = Row(
          children: [
            Text(photo.likes.toString(),
                textAlign: TextAlign.right, style: theme.textTheme.bodyText1),
            const SizedBox(width: 4),
            const Icon(Icons.favorite_border, color: Colors.pink),
          ],
        );

        return Card(
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: Hero(
                  tag: photo.id,
                  child: _buildImage(previewUrl, width, theme),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 8,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    userProfilePicture,
                    const SizedBox(width: 10),
                    Expanded(
                      child: userNameAndPhotoDate,
                    ),
                    likes,
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  CachedNetworkImage _buildImage(
    String previewUrl,
    double width,
    ThemeData theme,
  ) {
    return CachedNetworkImage(
      imageUrl: previewUrl,
      imageBuilder: (context, imageProvider) {
        return Stack(
          children: [
            AspectRatio(
              aspectRatio: photo.aspectRatio,
              child: SizedBox(
                width: width,
                child: Material(
                  child: Ink.image(
                    image: imageProvider,
                    child: InkWell(
                      onTap: () => Navigator.push(
                        context,
                        PageRouteBuilder(
                          opaque: false,
                          pageBuilder: (_, __, ___) {
                            return FullscreenPhoto(
                              photo: photo,
                              placeholderUrl: previewUrl,
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            if (photo.isSponsored)
              Positioned(
                top: 8,
                right: 8,
                // bottom: 8,
                // left: 8,
                child: Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    color: theme.accentColor,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  // child: Text(
                  //   'Sponsored',
                  //   style: theme.accentTextTheme.subtitle2,
                  // ),
                  child: Center(
                    child: Icon(
                      Icons.attach_money,
                      color: theme.accentIconTheme.color,
                      size: 18,
                      // color: theme.accentColor,
                    ),
                  ),
                ),
              ),
          ],
        );
      },
      placeholder: (_, __) => PhotoPlaceholder(photo: photo, width: width),
      errorWidget: (_, __, ___) => _buildErrorPlaceholder(theme, width),
    );
  }

  PhotoPlaceholder _buildErrorPlaceholder(ThemeData theme, double width) {
    final imageBrightness = ThemeData.estimateBrightnessForColor(photo.color);
    final textColorTheme = imageBrightness == Brightness.light
        ? theme.typography.black
        : theme.typography.white;
    final textBackgroundColor =
        imageBrightness == Brightness.light ? Colors.white38 : Colors.black38;

    Widget _errorTextBackground(Widget errorText) {
      return Container(
        decoration: BoxDecoration(
          color: textBackgroundColor,
          borderRadius: BorderRadius.circular(4),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
        child: errorText,
      );
    }

    return PhotoPlaceholder(
      photo: photo,
      width: width,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _errorTextBackground(
              Text(
                'Failed to load image.',
                style: theme.textTheme.bodyText1.copyWith(
                  color: textColorTheme.bodyText1.color,
                ),
              ),
            ),
            if (photo.description != null || photo.altDescription != null)
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: _errorTextBackground(
                  Text(
                    photo.description ?? photo.altDescription,
                    style: theme.textTheme.bodyText2.copyWith(
                      color: textColorTheme.bodyText2.color,
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import '../../../models/photo.dart';

class FullscreenPhoto extends StatefulWidget {
  const FullscreenPhoto({
    @required this.photo,
    @required this.placeholderUrl,
    Key key,
  }) : super(key: key);

  final Photo photo;
  final String placeholderUrl;

  static const double maxZoom = 4;

  @override
  _FullscreenPhotoState createState() => _FullscreenPhotoState();
}

class _FullscreenPhotoState extends State<FullscreenPhoto> {
  static const double dismissThreshold = 100;

  PhotoViewController _controller;

  int _pointersOnScreen = 0;

  bool _isDragging = false;
  double _dragStartPosition = 0;
  double _dragCurrentPosition = 0;

  Tween<double> _offsetTween = Tween(begin: 0, end: 0);

  @override
  void initState() {
    super.initState();
    _controller = PhotoViewController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final screenAspectRatio = screenSize.width / screenSize.height;
    final photoAspectRatio = widget.photo.width / widget.photo.height;
    final isPhotoTooWide = photoAspectRatio > screenAspectRatio;
    final imageWidth = isPhotoTooWide
        ? screenSize.width
        : screenSize.height * photoAspectRatio;
    final imageHeight = isPhotoTooWide
        ? screenSize.width / photoAspectRatio
        : screenSize.height;

    return TweenAnimationBuilder<double>(
      tween: _offsetTween,
      duration: _isDragging ? Duration.zero : const Duration(milliseconds: 150),
      // ignore: sort_child_properties_last
      child: _buildPhoto(
        context: context,
        screenSize: screenSize,
        imageWidth: imageWidth,
        imageHeight: imageHeight,
      ),
      builder: (context, offset, child) {
        final dismissProgress =
            (offset.abs() / dismissThreshold).clamp(0.0, 1.0);
        final backgroundOpacity = Curves.easeOut.transform(1 - dismissProgress);
        return Listener(
          behavior: HitTestBehavior.translucent,
          onPointerDown: _onPointerDown,
          onPointerCancel: _onPointerCancel,
          onPointerUp: (event) => _onPointerUp(context),
          onPointerMove: _onPointerMove,
          child: Stack(
            alignment: Alignment.center,
            fit: StackFit.expand,
            children: [
              ColoredBox(
                color: Colors.black.withOpacity(backgroundOpacity),
              ),
              Positioned(
                top: (screenSize.height - imageHeight) / 2 + offset,
                height: imageHeight,
                width: imageWidth,
                child: child,
              ),
            ],
          ),
        );
      },
    );
  }

  PhotoView _buildPhoto({
    @required BuildContext context,
    @required Size screenSize,
    @required double imageWidth,
    @required double imageHeight,
  }) {
    final dpr = MediaQuery.of(context).devicePixelRatio;
    final photoSize = screenSize * FullscreenPhoto.maxZoom / 2;
    final fullImageUrl = widget.photo.urls
        .fullWithCustomDpr(
          dpr: dpr,
          width: photoSize.width,
          height: photoSize.height,
        )
        .toString();
    final imageBrightness =
        ThemeData.estimateBrightnessForColor(widget.photo.color);
    final textColorTheme = imageBrightness == Brightness.light
        ? Theme.of(context).typography.black
        : Theme.of(context).typography.white;
    final backgroundColor =
        imageBrightness == Brightness.light ? Colors.white38 : Colors.black38;
    return PhotoView.customChild(
      controller: _controller,
      backgroundDecoration: const BoxDecoration(color: Colors.transparent),
      childSize: Size(imageWidth, imageHeight),
      customSize: MediaQuery.of(context).size,
      maxScale: PhotoViewComputedScale.contained * FullscreenPhoto.maxZoom,
      minScale: PhotoViewComputedScale.contained,
      heroAttributes: PhotoViewHeroAttributes(
        tag: widget.photo.id,
        flightShuttleBuilder: (flightContext, animation, flightDirection,
            fromHeroContext, toHeroContext) {
          return CachedNetworkImage(
            imageUrl: widget.placeholderUrl,
          );
        },
        transitionOnUserGestures: true,
      ),
      child: CachedNetworkImage(
        fadeInDuration: const Duration(milliseconds: 300),
        fadeOutDuration: const Duration(milliseconds: 300),
        placeholderFadeInDuration: Duration.zero,
        imageUrl: fullImageUrl,
        progressIndicatorBuilder: (context, url, progress) {
          return Column(
            children: [
              Expanded(
                child: CachedNetworkImage(
                  placeholderFadeInDuration: Duration.zero,
                  fadeInDuration: Duration.zero,
                  fadeOutDuration: Duration.zero,
                  imageUrl: widget.placeholderUrl,
                ),
              ),
              LinearProgressIndicator(
                value: progress.progress,
                minHeight: 1,
              ),
            ],
          );
        },
        errorWidget: (context, url, error) {
          return Stack(
            alignment: Alignment.center,
            children: [
              CachedNetworkImage(
                fadeInDuration: Duration.zero,
                fadeOutDuration: Duration.zero,
                placeholderFadeInDuration: Duration.zero,
                imageUrl: widget.placeholderUrl,
              ),
              _buildErrorText(
                context: context,
                textColorTheme: textColorTheme,
                backgroundColor: backgroundColor,
              ),
            ],
          );
        },
      ),
    );
  }

  void _onPointerMove(PointerMoveEvent event) {
    if (_pointersOnScreen == 1) {
      if (_isDragging) {
        _dragCurrentPosition = event.position.dy;
        final newOffset = _dragCurrentPosition - _dragStartPosition;
        _offsetTween = Tween(begin: newOffset, end: newOffset);

        setState(() {});
      }
    }
  }

  void _onPointerUp(BuildContext context) {
    if (_pointersOnScreen == 1) {
      if (_isDragging &&
          (_dragCurrentPosition - _dragStartPosition).abs() >=
              dismissThreshold) {
        Navigator.of(context).pop();
      } else if (_isDragging) {
        _isDragging = false;
        _dragStartPosition = 0;
        _dragCurrentPosition = 0;
        _offsetTween = Tween(begin: _offsetTween.begin, end: 0);
      }
    }
    _pointersOnScreen--;
    setState(() {});
  }

  void _onPointerCancel(PointerCancelEvent event) {
    if (_pointersOnScreen == 1) {
      if (_isDragging) {
        _isDragging = false;
        _dragStartPosition = 0;
        _dragCurrentPosition = 0;
        _offsetTween = Tween(begin: _offsetTween.begin, end: 0);
      }
    }
    _pointersOnScreen--;
    setState(() {});
  }

  void _onPointerDown(PointerDownEvent event) {
    _pointersOnScreen++;
    if (_pointersOnScreen == 1) {
      if ((_controller.scale - 1).abs() < 0.05) {
        _dragStartPosition = event.position.dy;
        _dragCurrentPosition = _dragStartPosition;
        _isDragging = true;
      }
    } else {
      _isDragging = false;
      _dragCurrentPosition = _dragStartPosition;
      _offsetTween = Tween(begin: _offsetTween.begin, end: 0);
    }
    setState(() {});
  }

  Widget _buildErrorText({
    @required BuildContext context,
    @required TextTheme textColorTheme,
    @required Color backgroundColor,
  }) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: backgroundColor,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
      child: Text(
        'Failed to load full image.',
        style: Theme.of(context).textTheme.headline6.copyWith(
              color: textColorTheme.headline6.color,
            ),
      ),
    );
  }
}

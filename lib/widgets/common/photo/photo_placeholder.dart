import 'package:flutter/material.dart';

import '../../../models/photo.dart';

class PhotoPlaceholder extends StatelessWidget {
  const PhotoPlaceholder({
    @required this.photo,
    this.width,
    this.height,
    this.child,
    Key key,
  })  : assert(
          width != null || height != null,
          'Neither width nor height is set.',
        ),
        super(key: key);

  final Photo photo;

  final double width;
  final double height;

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: photo.aspectRatio,
      child: Container(
        width: width,
        height: height,
        color: photo.color,
        child: child,
      ),
    );
  }
}
